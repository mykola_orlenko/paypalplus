<?php
namespace Iways\PayPalPlus\Api;

/**
 * Interface for managing placing order
 *
 * @api
 */
interface OrderManagementInterface
{
    /**
     * Placing order for guest
     * @param string $cartId
     * @param string $paymentId
     * @param string $paymentToken
     * @param string $payerId
     * @return \Iways\PayPalPlus\Api\Data\ResponseInterface
     */
    public function placeOrderGuest(string $cartId, string $paymentId, string $paymentToken, string $payerId);

    /**
     * Placing order for mine
     * @param string $cartId
     * @param string $paymentId
     * @param string $paymentToken
     * @param string $payerId
     * @return \Iways\PayPalPlus\Api\Data\ResponseInterface
     */
    public function placeOrderMine(string $cartId, string $paymentId, string $paymentToken, string $payerId);
}
