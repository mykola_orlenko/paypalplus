<?php

namespace Iways\PayPalPlus\Api\Data;

use Magento\Framework\DataObject;

interface ResponseInterface
{
    /**
     * Constants
     *
     * @var string
     */
    public const PAYPAL_PAYMENT_EXPERIENCE = 'paypalPaymentExperience';
    public const PAYPAL_PAYMENT_ID = 'paypalPaymentId';
    public const SHOW_PUI_ON_SANDBOX = 'showPuiOnSandbox';
    public const SHOW_LOADING_INDICATOR = 'showLoadingIndicator';
    public const MODE = 'mode';
    public const COUNTRY = 'country';
    public const LANGUAGE = 'language';
    public const THIRD_PARTY_PAYMENT_METHODS = 'thirdPartyPaymentMethods';
    public const ORDER_ID = 'orderId';
    public const ORDER_INCREMENT_ID = 'orderIncrementId';
    public const PAYMENT_ID = 'paymentId';
    public const ERROR = 'error';

    /**
     * Get paypalPaymentExperience
     *
     * @return string
     */
    public function getPaypalPaymentExperience();

    /**
     * Set paypalPaymentExperience
     *
     * @param  string $paypalPaymentExperience
     * @return $this
     */
    public function setPaypalPaymentExperience($paypalPaymentExperience);

    /**
     * Get paypalPaymentId
     *
     * @return string
     */
    public function getPaypalPaymentId();

    /**
     * Set paypalPaymentId
     *
     * @param  string $paypalPaymentId
     * @return $this
     */
    public function setPaypalPaymentId($paypalPaymentId);

    /**
     * Get showPuiOnSandbox
     *
     * @return bool
     */
    public function getShowPuiOnSandbox();

    /**
     * Set showPuiOnSandbox
     *
     * @param  bool $showPuiOnSandbox
     * @return $this
     */
    public function setShowPuiOnSandbox($showPuiOnSandbox);

    /**
     * Get showLoadingIndicator
     *
     * @return bool
     */
    public function getShowLoadingIndicatorx();

    /**
     * Set showLoadingIndicator
     *
     * @param  bool $showLoadingIndicator
     * @return $this
     */
    public function setShowLoadingIndicator($showLoadingIndicator);

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode();

    /**
     * Set mode
     *
     * @param  string $mode
     * @return $this
     */
    public function setMode($mode);

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry();

    /**
     * Set country
     *
     * @param  string $country
     * @return $this
     */
    public function setCountry($country);

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage();

    /**
     * Set language
     *
     * @param  string $language
     * @return $this
     */
    public function setLanguage($language);

    /**
     * Get thirdPartyPaymentMethods
     *
     * @return string
     */
    public function getThirdPartyPaymentMethods();

    /**
     * Set thirdPartyPaymentMethods
     *
     * @param  string $thirdPartyPaymentMethods
     * @return $this
     */
    public function setThirdPartyPaymentMethods($thirdPartyPaymentMethods);

    /**
     * Get orderId
     *
     * @return string
     */
    public function getOrderId();

    /**
     * Set orderId
     *
     * @param  string $orderId
     * @return $this
     */
    public function setOrderId($orderId);

    /**
     * Get orderIncrementId
     *
     * @return string
     */
    public function getOrderIncrementId();

    /**
     * Set orderId
     *
     * @param  string $orderIncrementId
     * @return $this
     */
    public function setOrderIncrementId($orderIncrementId);

    /**
     * Get paymentId
     *
     * @return string
     */
    public function getPaymentId();

    /**
     * Set paymentId
     *
     * @param  string $paymentId
     * @return $this
     */
    public function setPaymentId($paymentId);

    /**
     * Get error
     *
     * @return string
     */
    public function getError();

    /**
     * Set error
     *
     * @param  string $error
     * @return $this
     */
    public function setError($error);

}
