<?php

namespace Iways\PayPalPlus\Api;

/**
 * Interface for gathering guest payment information
 *
 * @api
 */
interface PaymentManagementInterface
{
    /**
     * Get payment config for guest
     * @param string $cartId
     * @return \Iways\PayPalPlus\Api\Data\ResponseInterface
     */
    public function getPaymentConfigGuest($cartId);

    /**
     * Get payment config for me
     * @param string $cartId
     * @return \Iways\PayPalPlus\Api\Data\ResponseInterface
     */
    public function getPaymentConfigMine($cartId);

    /**
     * Set payment information for a specified cart for guest.
     * @param string $cartId
     * @param string $paypalPaymentId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param string|null $email
     * @return \Iways\PayPalPlus\Api\Data\ResponseInterface
     */
    public function patchPaymentGuest(
        string $cartId,
        string $paypalPaymentId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        string $email = null);

    /**
     * Set payment information for a specified cart for me.
     * @param string $cartId
     * @param string $paypalPaymentId
     * @param \Magento\Quote\Api\Data\PaymentInterface $paymentMethod
     * @param string|null $email
     * @return \Iways\PayPalPlus\Api\Data\ResponseInterface
     */
    public function patchPaymentMine(
        string $cartId,
        string $paypalPaymentId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        string $email = null);

}
