<?php

declare(strict_types=1);

namespace Iways\PayPalPlus\Model;

use Iways\PayPalPlus\Api\Data\ResponseInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Response
 * Represents response
 */
class Response extends AbstractModel implements ResponseInterface
{
    /**
     * @inheritDoc
     */
    public function getPaypalPaymentExperience()
    {
        return $this->getData(ResponseInterface::PAYPAL_PAYMENT_EXPERIENCE);
    }

    /**
     * @inheritDoc
     */
    public function setPaypalPaymentExperience($paypalPaymentExperience)
    {
        $this->setData(ResponseInterface::PAYPAL_PAYMENT_EXPERIENCE, $paypalPaymentExperience);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPaypalPaymentId()
    {
        return $this->getData(ResponseInterface::PAYPAL_PAYMENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setPaypalPaymentId($paypalPaymentId)
    {
        $this->setData(ResponseInterface::PAYPAL_PAYMENT_ID, $paypalPaymentId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getShowPuiOnSandbox()
    {
        return $this->getData(ResponseInterface::SHOW_PUI_ON_SANDBOX);
    }

    /**
     * @inheritDoc
     */
    public function setShowPuiOnSandbox($showPuiOnSandbox)
    {
        $this->setData(ResponseInterface::SHOW_PUI_ON_SANDBOX, $showPuiOnSandbox);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getShowLoadingIndicatorx()
    {
        return $this->getData(ResponseInterface::SHOW_LOADING_INDICATOR);
    }

    /**
     * @inheritDoc
     */
    public function setShowLoadingIndicator($showLoadingIndicator)
    {
        $this->setData(ResponseInterface::SHOW_LOADING_INDICATOR, $showLoadingIndicator);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getMode()
    {
        return $this->getData(ResponseInterface::MODE);
    }

    /**
     * @inheritDoc
     */
    public function setMode($mode)
    {
        $this->setData(ResponseInterface::MODE, $mode);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCountry()
    {
        return $this->getData(ResponseInterface::COUNTRY);
    }

    /**
     * @inheritDoc
     */
    public function setCountry($country)
    {
        $this->setData(ResponseInterface::COUNTRY, $country);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLanguage()
    {
        return $this->getData(ResponseInterface::LANGUAGE);
    }

    /**
     * @inheritDoc
     */
    public function setLanguage($language)
    {
        $this->setData(ResponseInterface::LANGUAGE, $language);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getThirdPartyPaymentMethods()
    {
        return $this->getData(ResponseInterface::THIRD_PARTY_PAYMENT_METHODS);
    }

    /**
     * @inheritDoc
     */
    public function setThirdPartyPaymentMethods($thirdPartyPaymentMethods)
    {
        $this->setData(ResponseInterface::THIRD_PARTY_PAYMENT_METHODS, $thirdPartyPaymentMethods);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOrderId()
    {
        return $this->getData(ResponseInterface::ORDER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setOrderId($orderId)
    {
        $this->setData(ResponseInterface::ORDER_ID, $orderId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOrderIncrementId()
    {
        return $this->getData(ResponseInterface::ORDER_INCREMENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setOrderIncrementId($orderIncrementId)
    {
        $this->setData(ResponseInterface::ORDER_INCREMENT_ID, $orderIncrementId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPaymentId()
    {
        return $this->getData(ResponseInterface::PAYMENT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setPaymentId($paymentId)
    {
        $this->setData(ResponseInterface::PAYMENT_ID, $paymentId);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getError()
    {
        return $this->getData(ResponseInterface::ERROR);
    }

    /**
     * @inheritDoc
     */
    public function setError($error)
    {
        $this->setData(ResponseInterface::ERROR, $error);
        return $this;
    }

}
