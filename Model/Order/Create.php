<?php
namespace Iways\PayPalPlus\Model\Order;

use Iways\PayPalPlus\Model\Response;
use Magento\Customer\Model\Session;
use Magento\Framework\DataObject;
use Magento\Quote\Model\QuoteIdMaskFactory;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\OrderFactory;
use Iways\PayPalPlus\Api\OrderManagementInterface;

/**
 * PayPalPlus checkout controller
 *
 * @author robert
 */
class Create implements OrderManagementInterface
{
    const MAX_SEND_MAIL_VERSION = '2.2.6';
    /**
     * Protected $logger
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Protected $checkoutSession
     *
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * Protected $checkoutHelper
     *
     * @var \Magento\Checkout\Helper\Data
     */
    protected $checkoutHelper;

    /**
     * Protected $customerSession
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * Protected $cartManagement
     *
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $cartManagement;

    /**
     * Protected $guestCartManagement
     *
     * @var \Magento\Quote\Api\GuestCartManagementInterface
     */
    protected $guestCartManagement;

    /**
     * Protected $quoteIdMaskFactory
     *
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * Protected $orderFactory
     *
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderFactory;

    /**
     * Protected $cartRepository
     *
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * Protected $orderSender
     *
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $orderSender;

    /**
     * Protected $historyFactory
     *
     * @var \Magento\Sales\Model\Order\Status\HistoryFactory
     */
    protected $historyFactory;

    /**
     * Protected $productMetadata
     *
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;

    /**
     * @var \Iways\PayPalPlus\Model\ResponseFactory
     */
    protected $responseFactory;

    /**
     * Protected $request
     *
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Quote\Api\CartManagementInterface $cartManagement,
        \Magento\Quote\Api\GuestCartManagementInterface $guestCartManagement,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\Order\Status\HistoryFactory $historyFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Iways\PayPalPlus\Model\ResponseFactory $responseFactory
    ) {
        $this->request = $request;
        $this->logger = $logger;
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelper = $checkoutHelper;
        $this->cartManagement = $cartManagement;
        $this->quoteManagement = $quoteRepository;
        $this->guestCartManagement = $guestCartManagement;
        $this->customerSession = $customerSession;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->orderSender = $orderSender;
        $this->orderFactory = $orderFactory;
        $this->historyFactory = $historyFactory;
        $this->productMetadata = $productMetadata;
        $this->responseFactory = $responseFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function placeOrderGuest(string $cartId, string $paymentId, string $paymentToken, string $payerId)
    {
        $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
        $quote = $this->quoteManagement->getActive($quoteIdMask->getQuoteId());
        $response = $this->placeOrder($cartId, $paymentId, $paymentToken, $payerId, $this->guestCartManagement);
        return $response;
    }

    /**
     * {@inheritDoc}
     */
    public function placeOrderMine(string $cartId, string $paymentId, string $paymentToken, string $payerId)
    {
        $response = $this->placeOrder($cartId, $paymentId, $paymentToken, $payerId, $this->cartManagement);
        return $response;
    }

    /**
     * @param string $cartId
     * @param string $paymentId
     * @param string $token
     * @param string $payerId
     * @param object $cartManagement
     * @return \Iways\PayPalPlus\Api\Data\ResponseInterface
     */
    private function placeOrder(string $cartId, string $paymentId, string $paymentToken, string $payerId, object $cartManagement)
    {
        try {
            /** @var Response $response */
            $response = $this->responseFactory->create();

            $this->request->setParam('paymentId', $paymentId);
            $this->request->setParam('token', $paymentToken);
            $this->request->setParam('PayerID', $payerId);

            $orderId = $cartManagement->placeOrder($cartId);
            if ($orderId) {
                $response->setOrderId($orderId);
                $order = $this->orderFactory->create()->load($orderId);

                $response->setOrderId($orderId);
                $response->setOrderIncrementId($order->getIncrementId());

                if ($order->getCanSendNewEmailFlag()
                    && version_compare($this->productMetadata->getVersion(), self::MAX_SEND_MAIL_VERSION, '<')
                ) {
                    try {
                        $this->orderSender->send($order);
                    } catch (\Exception $e) {
                        $response->setError($e->getMessage());
                        $this->logger->critical($e);
                    }
                }
            }
            $result = new DataObject();
            $result->setData('success', true);
            $result->setData('error', false);
            $this->_eventManager->dispatch(
                'checkout_controller_onepage_saveOrder',
                [
                    'result' => $result,
                    'action' => $this
                ]
            );
        } catch (\Exception $e) {
            $response->setError($e->getMessage());
            $this->logger->critical($e);
        }
        return $response;
    }
}
