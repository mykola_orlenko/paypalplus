<?php

namespace Iways\PayPalPlus\Model;

use Iways\PayPalPlus\Api\PaymentManagementInterface;
use Iways\PayPalPlus\Model\ApiFactory;
use Iways\PayPalPlus\Model\MethodList;
use Iways\PayPalPlus\Model\Response;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Quote\Model\QuoteIdMask;
use Magento\Quote\Model\Quote;

class PaymentManagement implements PaymentManagementInterface
{
    /**
     * Protected $payPalPlusApiFactory
     *
     * @var \Iways\PayPalPlus\Model\ApiFactory
     */
    protected $payPalPlusApiFactory;

    /**
     * Protected $payPalPlusHelper
     *
     * @var \Iways\PayPalPlus\Helper\Data
     */
    protected $payPalPlusHelper;

    /**
     * Protected $quoteManagement
     *
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteManagement;

    /**
     * Protected $scopeConfig
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Protected $methodList
     *
     * @var \Iways\PayPalPlus\Model\MethodList
     */
    protected $methodList;

    /**
     * Url Builder
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * Protected $quoteIdMaskFactory
     *
     * @var \Magento\Quote\Model\QuoteIdMaskFactory
     */
    protected $quoteIdMaskFactory;

    /**
     * @var \Iways\PayPalPlus\Model\ResponseFactory
     */
    protected $responseFactory;

    /**
     * Protected $cartRepository
     *
     * @var CartRepositoryInterface
     */
    protected $cartRepository;

    /**
     * Protected $paymentMethodManagement
     *
     * @var \Magento\Quote\Api\PaymentMethodManagementInterface
     */
    protected $paymentMethodManagement;

    /**
     * Protected $customerSession
     *
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * Protected $messageManager
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    public function __construct(
        \Iways\PayPalPlus\Model\ApiFactory $payPalPlusApiFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\Session $customerSession,
        \Iways\PayPalPlus\Helper\Data $payPalPlusHelper,
        \Magento\Quote\Api\PaymentMethodManagementInterface $paymentMethodManagement,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Iways\PayPalPlus\Model\MethodList $methodList,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Quote\Model\QuoteIdMaskFactory $quoteIdMaskFactory,
        \Iways\PayPalPlus\Model\ResponseFactory $responseFactory)
    {
        $this->payPalPlusApiFactory = $payPalPlusApiFactory;
        $this->messageManager = $messageManager;
        $this->customerSession = $customerSession;
        $this->payPalPlusHelper = $payPalPlusHelper;
        $this->paymentMethodManagement = $paymentMethodManagement;
        $this->quoteManagement = $quoteRepository;
        $this->scopeConfig = $scopeConfig;
        $this->methodList = $methodList;
        $this->urlBuilder = $urlBuilder;
        $this->quoteIdMaskFactory = $quoteIdMaskFactory;
        $this->responseFactory = $responseFactory;
    }

    /**
     * {@inheritDoc}
     */
    public function getPaymentConfigGuest($cartId)
    {
        /** @var Response $response */
        $response = $this->responseFactory->create();

        try {
            /** @var QuoteIdMask $quoteId */
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
            /** @var Quote $quote */
            $quote = $this->quoteManagement->getActive($quoteIdMask->getQuoteId());
            $response = $this->getConfigForPaymentByQuote($quote);
        } catch (\Exception $e) {
            $response->setError($e->getMessage());
        }

        return $response;
    }

    /**
     * {@inheritDoc}
     */
    public function getPaymentConfigMine($cartId)
    {
        /** @var Response $response */
        $response = $this->responseFactory->create();

        try {
            /** @var Quote $quote */
            $quote = $this->quoteManagement->getActive($cartId);
            $response = $this->getConfigForPaymentByQuote($quote);
        } catch (\Exception $e) {
            $response->setError($e->getMessage());
        }

        return $response;
    }

    /**
     * {@inheritDoc}
     */
    public function patchPaymentGuest(
        string $cartId,
        string $paypalPaymentId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        string $email = null)
    {
        /** @var Response $response */
        $response = $this->responseFactory->create();

        try {
            /** @var QuoteIdMask $quoteId */
            $quoteIdMask = $this->quoteIdMaskFactory->create()->load($cartId, 'masked_id');
            /** @var Quote $quote */
            $quote = $this->quoteManagement->getActive($quoteIdMask->getQuoteId());

            $quote->getBillingAddress()->setEmail($email);
            $paymentId = $this->paymentMethodManagement->set($quote->getId(), $paymentMethod);
            $response->setPaymentId($paymentId);

            $this->customerSession->setPayPalPaymentId($paypalPaymentId);
            $this->customerSession->setPayPalPaymentPatched(null);
            $this->payPalPlusApiFactory->create()->patchPayment($quote);
        } catch (\Exception $e) {
            $response->setError($e->getMessage());
        }

        return $response;
    }

    /**
     * {@inheritDoc}
     */
    public function patchPaymentMine(
        string $cartId,
        string $paypalPaymentId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        string $email = null)
    {
        /** @var Response $response */
        $response = $this->responseFactory->create();

        try {
            /** @var Quote $quote */
            $quote = $this->quoteManagement->getActive($cartId);

            $paymentId = $this->paymentMethodManagement->set($quote->getId(), $paymentMethod);
            $response->setPaymentId($paymentId);

            $this->customerSession->setPayPalPaymentId($paypalPaymentId);
            $this->customerSession->setPayPalPaymentPatched(null);
            $this->payPalPlusApiFactory->create()->patchPayment($quote);
        } catch (\Exception $e) {
            $response->setError($e->getMessage());
        }

        return $response;
    }

    /**
     * Creates new paypalplus payment paymentExperience url using PayPalPlus API
     * returns response for Magento API
     * @param \Magento\Quote\Model\Quote $quote
     * @return \Iways\PayPalPlus\Api\Data\ResponseInterface
     */
    private function getConfigForPaymentByQuote($quote)
    {
        /** @var Response $response */
        $response = $this->responseFactory->create();

        $paymentExperience = $this->payPalPlusHelper->getPaymentExperienceByQuote($quote);
        if ($paymentExperience) {
            $response->setPaypalPaymentExperience($paymentExperience);
        }
        else {
            if (count($this->messageManager->getMessages()->getItems()) && $this->messageManager->getMessages()->getLastAddedMessage()) {
                $message = $this->messageManager->getMessages()->getLastAddedMessage()->getText();
                $response->setError($message);
            }
            else {
                $response->setError(__("Error. Please check Paypal configuration."));
            }
        }

        if($this->customerSession->getPayPalPaymentId()){
            $response->setPaypalPaymentId($this->customerSession->getPayPalPaymentId());
        }

        $showPuiOnSandbox = $this->scopeConfig->getValue(
            'iways_paypalplus/dev/pui_sandbox',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ) ? true : false;

        $showLoadingIndicator = $this->scopeConfig->getValue(
            'payment/iways_paypalplus_payment/show_loading_indicator',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ) ? true : false;

        $mode = $this->scopeConfig->getValue(
            'iways_paypalplus/api/mode',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $language = $this->scopeConfig->getValue(
            'general/locale/code',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $response->setShowPuiOnSandbox($showPuiOnSandbox);
        $response->setShowLoadingIndicator($showLoadingIndicator);
        $response->setMode($mode);
        $response->setCountry($this->getCountry($quote));
        $response->setLanguage($language);
        $response->setThirdPartyPaymentMethods($this->getThirdPartyMethods($quote));

        return $response;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return string
     */
    private function getCountry($quote)
    {
        $billingAddress = $quote->getBillingAddress();
        if ($billingAddress->getCountryId()) {
            return $billingAddress->getCountryId();
        }

        $shippingAddress = $quote->getShippingAddress();
        if ($shippingAddress->getCountryId()) {
            return $shippingAddress->getCountryId();
        }

        return $this->scopeConfig->getValue(
            'paypal/general/merchant_country',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    private function getThirdPartyMethods($quote)
    {
        $this->methodList->setCheckPPP(true);
        $paymentMethods = $this->methodList->getAvailableMethods($quote);
        $this->methodList->setCheckPPP(false);
        $allowedPPPMethods = explode(
            ',',
            $this->scopeConfig->getValue(
                'payment/iways_paypalplus_payment/third_party_moduls',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        );
        $methods = [];
        foreach ($paymentMethods as $paymentMethod) {
            if (strpos($paymentMethod->getCode(), 'paypal') === false
                && in_array($paymentMethod->getCode(), $allowedPPPMethods)) {
                if ($methodImage = $this->scopeConfig->getValue(
                    'payment/iways_paypalplus_section/third_party_modul_info_image_' . $paymentMethod->getCode(),
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                )) {
                    if (substr($methodImage, 0, 4) != 'http') {
                        $methodImage = $this->assetRepo->getUrl($methodImage);
                    }
                }
                $method = [
                    'redirectUrl' => $this->urlBuilder->getUrl('checkout', ['_secure' => true]),
                    'code'        => $paymentMethod->getCode(),
                    'methodName'  => $paymentMethod->getTitle(),
                    'imageUrl'    => $methodImage,
                    'description' => $this->scopeConfig->getValue(
                        'payment/iways_paypalplus_section/third_party_modul_info_text_' . $paymentMethod->getCode(),
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    ),
                ];
                $methods[] = $method;
            }
        }
        return $methods;
    }
}
